*How To Use*
Send the code for state related information
All States - Covid@TT
Andaman and Nicobar Islands - Covid@AN
Andhra Pradesh -Covid@AP
Arunachal Pradesh - Covid@AR
Assam - Covid@AS
Bihar - Covid@BR
Chandigarh - Covid@CH
Chhattisgarh - Covid@CT
Dadra and Nagar Haveli - Covid@DN
Daman and Diu - Covid@DD
Delhi - Covid@DL
Goa - Covid@GA
Gujarat - Covid@GJ
Haryana - Covid@HR
Himachal Pradesh - Covid@HP
Jammu and Kashmir - Covid@JK
Jharkhand - Covid@JH
Karnataka - Covid@KA
Kerala - Covid@KL
Ladakh - Covid@LA
Lakshadweep - Covid@LD
Madhya Pradesh - Covid@MP
Maharashtra - Covid@MH
Manipur - Covid@MN
Meghalaya - Covid@ML
Mizoram - Covid@MZ
Nagaland - Covid@NL
Odisha - Covid@OR
Puducherry - Covid@PY
Punjab - Covid@PB
Rajasthan - Covid@RJ
Sikkim - Covid@SK
Tamil Nadu - Covid@TN
Telangana - Covid@TG
Tripura - Covid@TR
Uttar Pradesh - Covid@UP
Uttarakhand - Covid@UT
West Bengal - Covid@WB

Example for Andhra Pradesh State : *Covid-19@AP* or *Covid@AP*