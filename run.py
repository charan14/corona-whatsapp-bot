from io import BytesIO
import json, os, sys
from selenium.webdriver.common.keys import Keys
import time
import pyperclip
import requests
import pandas as pd
import matplotlib.pyplot as plt
import win32clipboard
from PIL import Image
import urllib.request
from selenium import webdriver
from selenium.webdriver import ActionChains
from win10toast import ToastNotifier
import subprocess

def send_to_clipboard(clip_type, data):
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardData(clip_type, data)
    win32clipboard.CloseClipboard()

def read_file(file):
    fo = open(file, 'r').read()
    pyperclip.copy(fo)

def state_details(state):

    url = 'https://api.covid19india.org/data.json'
    response = requests.get(url)
    data = json.loads(response.text)

    text = ''

    for st_code in state:
        for i in range(len(data['statewise'])):
            if data['statewise'][i]['statecode'] == st_code:
                if data['statewise'][i]['state'] == 'Total':
                    data['statewise'][i]['state'] = 'All States'
                text = text + '' + '*State wise Covid - 19 Cases in ' + data['statewise'][i]['state'] + '*\n' \
                                                                                                        'State Name: ' + \
                       data['statewise'][i]['state'] + '\n' \
                                                       'Total Confirmed Cases : ' + data['statewise'][i][
                           'confirmed'] + '\n' \
                                          'Total Active Cases : ' + str(int(data['statewise'][i]['confirmed']) - (
                        int(data['statewise'][i]['recovered']) + int(data['statewise'][i]['deaths']))) + '\n' \
                                                                                                         'Total Recovered : ' + \
                       data['statewise'][i]['recovered'] + '\n' \
                                                           'Total deaths : ' + data['statewise'][i]['deaths'] + '\n' \
                                                                                                                'New Cases : ' + \
                       data['statewise'][i]['deltaconfirmed'] + '\n' \
                                                                'New Recovered : ' + data['statewise'][i][
                           'deltarecovered'] + '\n' \
                                               'New Deaths : ' + data['statewise'][i]['deltadeaths'] + '\n' \
                                                                                                       'Last Updated On: ' + \
                       data['statewise'][i]['lastupdatedtime'] + '\n'
                if st_code != 'TT':
                    text = text + 'For district wise data in ' + data['statewise'][i]['statecode'] + ' : *Covid@' + \
                           data['statewise'][i]['statecode'] + '-All*\n'
                text = text + 'For Graph Representation : ' + data['statewise'][i]['statecode'] + ' : *Covid@' + \
                       data['statewise'][i]['statecode'] + '-Graph* \n\n'

    os.remove('text.txt')
    with open('text.txt', 'a') as the_file:
        the_file.write(text)
    return ('text.txt')

def dist_details(state):

    url = 'https://api.covid19india.org/data.json'
    response = requests.get(url)
    data = json.loads(response.text)
    url = 'https://api.covid19india.org/state_district_wise.json'
    response = requests.get(url)
    data1 = json.loads(response.text)
    url = 'https://api.covid19india.org/zones.json'
    response = requests.get(url)
    data2 = json.loads(response.text)

    text = ''

    for s in state:

        for i in range(len(data['statewise'])):
            if data['statewise'][i]['statecode'] == s:
                st = data['statewise'][i]['state']
                lu = data['statewise'][i]['lastupdatedtime']
                dist = data1[st]['districtData'].keys()
                # print(st)
                zone = '{'

                for m in range(0, 734):
                    if data2['zones'][m]['state'] == st:
                        zone = zone + '"' + data2['zones'][m]['district'] + '":"' + data2['zones'][m]['zone'] + '",'

                zone = zone[:-1] + "}"
                zone = json.loads(zone)
                text = text + '*Covid 19 ' + st + ' Details*\n'
                for dis in dist:
                    try:
                        text = text + str(dis) + ' ' + str(
                            data1[st]['districtData'][dis]['confirmed']) + ' Cases(' + str(
                            data1[st]['districtData'][dis]['delta']['confirmed']) + ' New) - ' + zone[
                                   dis] + ' Zone\n'
                    except:
                        text = text + str(dis) + ' ' + str(
                            data1[st]['districtData'][dis]['confirmed']) + ' Cases(' + str(
                            data1[st]['districtData'][dis]['delta']['confirmed']) + ' New) \n'

                text = text + '\nTotal Cases : ' + data['statewise'][i][
                    'confirmed'] + '\n' + 'Total Active Cases : ' + str(int(data['statewise'][i]['confirmed']) - (
                        int(data['statewise'][i]['recovered']) + int(
                    data['statewise'][i]['deaths']))) + '\nTotal Recovered : ' + data['statewise'][i][
                           'recovered'] + '\n\n'

                text = text + 'Last Updated ' + lu + '\n'
                text = text + 'For Graph Representation of cases in ' + st + ' State Try *Covid@' + s + '-Graph* \n\n'
    # print(text)

    os.remove('text.txt')
    with open('text.txt', 'a') as the_file:
        the_file.write(text)
    return ('text.txt')

def state_graph(state):
    url = 'https://api.covid19india.org/data.json'
    response = requests.get(url)
    data = json.loads(response.text)

    url = 'http://api.covid19india.org/states_daily_csv/confirmed.csv'
    r = requests.get(url, allow_redirects=True)
    open('confirmed.csv', 'wb').write(r.content)

    url = 'http://api.covid19india.org/states_daily_csv/deceased.csv'
    r = requests.get(url, allow_redirects=True)
    open('deceased.csv', 'wb').write(r.content)

    url = 'http://api.covid19india.org/states_daily_csv/recovered.csv'
    r = requests.get(url, allow_redirects=True)
    open('recovered.csv', 'wb').write(r.content)

    ret_list = []

    for st_code in state:
        for i in range(len(data['statewise'])):
            if data['statewise'][i]['statecode'] == st_code:

                confirmed = pd.read_csv("confirmed.csv", usecols=['date', st_code])
                deceased = pd.read_csv("deceased.csv", usecols=[st_code])
                recovered = pd.read_csv("recovered.csv", usecols=[st_code])

                confirmed['deceased'] = deceased
                confirmed['recovered'] = recovered
                confirmed['active'] = confirmed[st_code] - (confirmed['recovered'] + confirmed['deceased'])

                confirmed.fillna(int(0), inplace=True)
                confirmed.rename(columns={st_code: 'confirmed'}, inplace=True)

                confirmed['confirmed'] = confirmed['confirmed'].cumsum()
                confirmed['deceased'] = confirmed['deceased'].cumsum()
                confirmed['recovered'] = confirmed['recovered'].cumsum()
                confirmed['active'] = confirmed['active'].cumsum()

                plt.figure()
                plt.subplot()
                plt.plot(confirmed['date'], confirmed['confirmed'], 'b', label='Confirmed')
                plt.plot(confirmed['date'], confirmed['deceased'], 'r', label='Died')
                plt.plot(confirmed['date'], confirmed['recovered'], 'g', label='Recovered')
                plt.legend(loc=2)
                plt.xticks(rotation=90)
                plt.xlabel(data['statewise'][i]['state']+ ' State line Plot Graph', labelpad=20)
                plt.tight_layout(h_pad=0.2, w_pad=0.2)
                plt.savefig('graphs/' + st_code + '.jpg', format="jpg")

                ret_list.append('graphs/' + st_code + '.jpg')

                plt.figure()
                plt.subplot()
                plt.plot(confirmed['date'], confirmed['active'], 'b', label='Active')
                plt.legend(loc=2)
                plt.xticks(rotation=90)
                plt.xlabel(data['statewise'][i]['state']+ ' State Active Cases line Plot Graph', labelpad=20)
                plt.tight_layout(h_pad=0.2, w_pad=0.2)
                plt.savefig('graphs/' + st_code + '-Active.jpg', format="jpg")

                ret_list.append('graphs/' + st_code + '-Active.jpg')

    return(ret_list)


ign_arc_list = ['']

driver = webdriver.Firefox()
actionChains = ActionChains(driver)
driver.get("https://web.whatsapp.com/")
print("Press Enter After Scaning Code")
time.sleep(1)

while True:
    try:

        #Get all chats
        names = []
        all_names = driver.find_elements_by_xpath("//span[@dir='auto']")
        for name in all_names:
            try:
                n = name.get_attribute('title')
                if n:
                    names.append(n)
            except:
                pass

        for name in names:
            try:
                person = driver.find_element_by_xpath('//span[@title="{}"]'.format(name))
                person.click()
                for i in range(1, 3):
                    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                chat = driver.find_elements_by_css_selector("span.selectable-text.invisible-space.copyable-text")
                user = chat[-1].find_element_by_xpath("..").find_element_by_xpath("..")
                user_text = user.get_attribute("data-pre-plain-text")
                user_name = user_text[20:-2].replace(' ', '')

                if user_text[20:25] != 'Covid':
                    msg = [message.text for message in chat]
                    msg[-1] = msg[-1].upper()
                    message = msg[-1].replace(' ', '').split('@')
                    state_list = ['TT', 'MH', 'N', 'DL', 'KL', 'TG', 'UP', 'RJ', 'AP', 'MP', 'K', 'GJ', 'JK', 'HR', 'PB',
                                'WB', 'BR', 'AS', 'UT', 'OR', 'CH', 'LA', 'AN', 'CT', 'GA', 'HP', 'PY', 'JH', 'MN',
                                'MZ','AR', 'DN', 'DD', 'LD', 'ML', 'NL', 'SK', 'TR']
                    covid_list = ['Covid-19', 'covid-19', 'covid 19', 'Covid 19', 'Covid', 'covid', 'Corona', 'corona',
                                  'COVID', 'COVID-19']
                    thank_list= ['TQ', 'THANKS', 'THANKYOU', 'TU', 'THANKU', 'OKAY', 'KK', 'GOOD', 'GREAT', 'NICE', 'WORK']
                    hi_list = ['HI', 'HII', 'HIII', 'HEY', 'HELLO']
                    graph_list = ['GRAPH','GRAPHS']

                    reply = driver.find_elements_by_class_name("_2S1VP.copyable-text.selectable-text")
                    reply[1].click()

                    state = []

                    if message[0] in covid_list:
                        try:
                            win32clipboard.OpenClipboard()
                            data = win32clipboard.GetClipboardData()
                            win32clipboard.CloseClipboard()
                        except:
                            win32clipboard.OpenClipboard()
                            win32clipboard.EmptyClipboard()
                            win32clipboard.SetClipboardText(' ')
                            win32clipboard.CloseClipboard()

                        if '-' in message[1]:
                            user_msg = message[1].split('-')

                            state.append(user_msg[0])

                            if user_msg[0] == 'ALL':
                                user_msg[0] = 'TT'
                            if user_msg[1] in graph_list and user_msg[0] in state_list: #Graphs
                                responce = state_graph(state)
                                for img in responce:
                                    image = Image.open(img)
                                    output = BytesIO()
                                    image.convert("RGB").save(output, "BMP")
                                    dat = output.getvalue()[14:]
                                    output.close()
                                    send_to_clipboard(win32clipboard.CF_DIB, dat)

                                    reply[1].send_keys(Keys.CONTROL, 'v')
                                    time.sleep(1)
                                    send = driver.find_element_by_xpath(
                                        '//*[@id="app"]/div/div/div[2]/div[2]/span/div/span/div/div/div[2]/span/div/div')
                                    send.click()

                                    time.sleep(1)
                                reply[1].send_keys('Graphs')
                                reply[1].send_keys(Keys.RETURN)

                            elif user_msg[1] == 'ALL' and user_msg[0] in state_list: #All District

                                win32clipboard.OpenClipboard()
                                data = win32clipboard.GetClipboardData()
                                win32clipboard.CloseClipboard()

                                read_file(dist_details(state))
                                reply[1].send_keys(Keys.CONTROL, 'v')
                                reply[1].send_keys(Keys.RETURN)

                                win32clipboard.OpenClipboard()
                                win32clipboard.EmptyClipboard()
                                win32clipboard.SetClipboardText(data)
                                win32clipboard.CloseClipboard()
                            else: #How
                                win32clipboard.OpenClipboard()
                                data = win32clipboard.GetClipboardData()
                                win32clipboard.CloseClipboard()

                                read_file('how.txt')
                                reply[1].send_keys(Keys.CONTROL, 'v')
                                reply[1].send_keys(Keys.RETURN)

                                win32clipboard.OpenClipboard()
                                win32clipboard.EmptyClipboard()
                                win32clipboard.SetClipboardText(data)
                                win32clipboard.CloseClipboard()

                        elif message[1] in state_list:  # State
                            state.append(message[1])

                            win32clipboard.OpenClipboard()
                            data = win32clipboard.GetClipboardData()
                            win32clipboard.CloseClipboard()

                            read_file(state_details(state))
                            reply[1].send_keys(Keys.CONTROL, 'v')
                            reply[1].send_keys(Keys.RETURN)

                            win32clipboard.OpenClipboard()
                            win32clipboard.EmptyClipboard()
                            win32clipboard.SetClipboardText(data)
                            win32clipboard.CloseClipboard()

                        else:  # How
                            win32clipboard.OpenClipboard()
                            data = win32clipboard.GetClipboardData()
                            win32clipboard.CloseClipboard()

                            read_file('how.txt')
                            reply[1].send_keys(Keys.CONTROL, 'v')
                            reply[1].send_keys(Keys.RETURN)

                            win32clipboard.OpenClipboard()
                            win32clipboard.EmptyClipboard()
                            win32clipboard.SetClipboardText(data)
                            win32clipboard.CloseClipboard()


                    elif message[0] in thank_list:
                        reply[1].send_keys('Please Share 🙂')
                        reply[1].send_keys(Keys.RETURN)

                    elif message[0] in hi_list:
                        reply[1].send_keys('Hello, Try *Covid@HI* ')
                        reply[1].send_keys(Keys.RETURN)

                    else:
                        reply[1].send_keys('For Corona related details please try *Covid@HI* ')
                        reply[1].send_keys(Keys.RETURN)

                    if user_name not in ign_arc_list:
                        time.sleep(2)
                        try:
                            ActionChains(driver).move_to_element(
                                driver.find_element_by_xpath(
                                    '//span[@title="{}"]'.format(name))).context_click().perform()
                            ActionChains(driver).send_keys(Keys.ARROW_DOWN).perform()
                            ActionChains(driver).send_keys(Keys.ENTER).perform()
                            with open("log.txt", "a") as myfile:

                                myfile.write(user_text + '\n')
                            print(user_text)
                        except:
                            print("Not Logged")

                    time.sleep(1)
                    names.remove(name)

            except:
                pass
    except:
        # use any sms api to know if script stoped working.
        print("Stoped")
        sys.exit()

