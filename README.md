# Corona Whatsapp Bot

This is Corona whatsapp bot which shares active corona cases status in whatsapp.

# Requirements
*  Python 3
*  Firefox
*  Missing Packages

# How To Use ?
*  Run "run.py"
*  Scan the Whatsapp web QR Code from whatsapp mobile app
*  Done, Its running.

# Contact
* Name : Saithana Sri Charan
* Email : charan.saithana@gmail.com
* LinkedIn : [www.linkedin.com/in/saithana-sri-charan](www.linkedin.com/in/saithana-sri-charan)

#Don't Forget to drop a mail if you have any queries.